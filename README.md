# Magento 2 - REST API module for Autocomplete #

This README would normally document whatever steps are necessary to get your application up and running.

## Usage

Like described on [GitHub](https://github.com/arvatoSCM/dockerize-magento2):

Start the docker containers:

```bash
sudo ./bin/console start
```

Stop the docker containers:

```bash
sudo ./bin/console stop
```

Execute `bin/magento` inside the docker container:

```bash
sudo ./bin/console exec [arguments]
```
### Database

Dump database: ./db.sql

### Credentials

All credentials (including docker containers and magento admin) is stored in ./.env file in root directory.

### Endpoint

```url
<domain>/rest/product/autocomplete/<product_name>
```