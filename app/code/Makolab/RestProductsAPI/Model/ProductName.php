<?php
namespace Makolab\RestProductsAPI\Model;
use Makolab\RestProductsAPI\Api\ProductsAutocompleteInterface;
 
class ProductName implements ProductsAutocompleteInterface
{
    private $_productCollectionFactory;
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context, 
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;        
    }
    /**
     * Returns products name for autocomplete
     *
     * @api
     * @param string $name Product name.
     * @return string Products name list.
     */
    public function getProductNameForAutocomplete($name) {
        $collection = $this->_productCollectionFactory->create()
            ->addAttributeToSelect(['id','name'])
            ->addAttributeToFilter('name', ['like'=>$name.'%']);
            //->addAttributeToFilter('featured_product', '1');
            //$collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        
        $productListName = [];

        foreach ($collection as $product) {
            //$aProduct = $product->getData();
            $productListName[$product->getId()] = ['product_id' => $product->getId(), 'name'=>$product->getName()];
        }
        return $productListName;
    }
}