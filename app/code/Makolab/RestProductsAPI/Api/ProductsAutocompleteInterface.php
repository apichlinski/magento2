<?php
namespace Makolab\RestProductsAPI\Api;
 
interface ProductsAutocompleteInterface
{
    /**
     * Returns products name for autocomplete
     *
     * @api
     * @param string $name Product name.
     * @return string Products name list.
     */
    public function getProductNameForAutocomplete($name);
}